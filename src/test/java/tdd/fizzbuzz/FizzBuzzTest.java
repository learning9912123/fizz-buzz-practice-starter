package tdd.fizzbuzz;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FizzBuzzTest {

    @Test
    public void should_return_normal_number_when_countOff_given_normal_number(){
        int number = 2;
        FizzBuzz fizzBuzz = new FizzBuzz();
        String res = fizzBuzz.countOff(number);
        Assertions.assertEquals("2", res);
    }

    @Test
    public void should_return_Fizz_when_countOff_given_multiple_of_3(){
        int number = 9;
        FizzBuzz fizzBuzz = new FizzBuzz();
        String res = fizzBuzz.countOff(number);
        Assertions.assertEquals("Fizz", res);
    }

    @Test
    public void should_return_Buzz_when_countOff_given_multiple_of_5(){
        int number = 10;
        FizzBuzz fizzBuzz = new FizzBuzz();
        String res = fizzBuzz.countOff(number);
        Assertions.assertEquals("Buzz", res);
    }

    @Test
    public void should_return_Buzz_when_countOff_given_multiple_of_3_and_5(){
        int number = 15;
        FizzBuzz fizzBuzz = new FizzBuzz();
        String res = fizzBuzz.countOff(number);
        Assertions.assertEquals("FizzBuzz", res);
    }

    @Test
    public void should_return_Buzz_when_countOff_given_multiple_of_7(){
        int number = 14;
        FizzBuzz fizzBuzz = new FizzBuzz();
        String res = fizzBuzz.countOff(number);
        Assertions.assertEquals("Whizz", res);
    }

    @Test
    public void should_return_Buzz_when_countOff_given_multiple_of_3_and_7(){
        int number = 21;
        FizzBuzz fizzBuzz = new FizzBuzz();
        String res = fizzBuzz.countOff(number);
        Assertions.assertEquals("FizzWhizz", res);
    }

    @Test
    public void should_return_Buzz_when_countOff_given_multiple_of_5_and_7(){
        int number = 35;
        FizzBuzz fizzBuzz = new FizzBuzz();
        String res = fizzBuzz.countOff(number);
        Assertions.assertEquals("BuzzWhizz", res);
    }

    @Test
    public void should_return_Buzz_when_countOff_given_multiple_of_3_and_5_and_7(){
        int number = 105;
        FizzBuzz fizzBuzz = new FizzBuzz();
        String res = fizzBuzz.countOff(number);
        Assertions.assertEquals("FizzBuzzWhizz", res);
    }


}
