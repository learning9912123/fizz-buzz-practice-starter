package tdd.fizzbuzz;

import java.util.HashMap;
import java.util.Map;

public class FizzBuzz {

    Map<Integer, String> map;

    public FizzBuzz() {
        this.map = new HashMap<>();
        map.put(3, "Fizz");
        map.put(5, "Buzz");
        map.put(7, "Whizz");
    }

    public String countOff(int number) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<Integer, String> entry : map.entrySet()) {
            int curr = entry.getKey();
            if (number % curr == 0) {
                sb.append(entry.getValue());
                number /= curr;
            }
        }
        if (sb.length() == 0) {
            return String.valueOf(number);
        }
        return sb.toString();
    }
}
